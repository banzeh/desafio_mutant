const _EXPRESS_PORT=3000;
const express = require("express");
const logMiddleware = require("express").Router();
const app = express();

const usersRoutes = require('./src/routes/users');

logMiddleware.use(function (req, res, next) {
  console.log("/" + req.method);
  next();
});

app.use(logMiddleware)
app.use('/', usersRoutes);

app.listen(_EXPRESS_PORT, function () {
  console.log(`Example app listening on port ${_EXPRESS_PORT}!`);
});
