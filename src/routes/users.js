const router = require("express").Router();
const Users = require('../controllers/users');
const users = new Users();

/** Retorna os usuários */
router.get('/users', (req, res, next) => {
  users.get()
    .then((data) => res.json(data))
    .catch((err) => res.statusCode(500).send(err));
});

/** Retorna nome, email e empresa */
router.get('/workplace', (req, res) => {
  users.getUsersInfo()
    .then((data) => res.json(data))
    .catch((err) => res.statusCode(500).send(err));
});

/** Retorna os websites de todos os usuários */
router.get('/websites', (req, res) => {
  users.getWebsites()
    .then((data) => res.json(data))
    .catch((err) => res.statusCode(500).send(err));
});

/** Filtra o endereço dos usuários */
router.get('/filter/:suite?', (req, res) => {
  const filter = req.params.suite || '';
  users.filterAddress(filter)
    .then((data) => res.json(data))
    .catch((err) => res.statusCode(500).send(err));
});

exports = module.exports = router;