const util = require('../util');

function Users() {
  this.url = "https://jsonplaceholder.typicode.com/users";
  this.usersData = [];
}

/** Faz o download do arquivo JSON e o mantém em memória */
Users.prototype.get = function() {
  return new Promise((resolve, reject) => {
    try {
      util.httpsGet(this.url)
        .then((data) => this.usersData = data)
        .then(resolve)
    } catch(err) {
      return reject(err);
    }
  })
}

/** Verifica se já foi feito o download do arquivo json e o mantém em memória */
Users.prototype.checkData = async function(){
  return new Promise(async (resolve, reject) => {
    try{
      if (this.usersData.length < 1)
        this.usersData = await this.get().catch(reject);
      return resolve();
    } catch(err) {
      return reject(err);
    }
  })
}

/** Retorna os websites do arquivo JSON */
Users.prototype.getWebsites = function() {
  return new Promise(async (resolve, reject) => {
    try{
      await this.checkData();
      let websites = this.usersData.map((item) => item.website)
      return resolve(websites);
    } catch(err) {
      return reject(err);
    }
  })
}

/** Retorna as informações dos usuários */
Users.prototype.getUsersInfo = function() {
  return new Promise(async (resolve, reject) => {
    try {
      await this.checkData();
      let websites = this.usersData
        .map((item) => { return { name: item.name, email: item.email, company: item.company.name }})
        .sort((a, b) => (a.name > b.name) ? 1 : -1);

      return resolve(websites);
    } catch(err) {
      return reject(err);
    }
  })
}

/** Filtra o endereço  */
Users.prototype.filterAddress = async function(filter) {
  return new Promise(async (resolve, reject) => {
    await this.checkData();
    let usersSuite = this.usersData
      .filter((item) => {
        let regexp_filter = new RegExp(filter, "gi");
        if ( regexp_filter.test(item.address.suite) || regexp_filter.test(item.address.street) )
          return item;
      })

    return resolve(usersSuite);
  })
}

module.exports = Users;