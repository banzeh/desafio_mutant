const chai = require('chai');
const Users = require('./users');
const users = new Users();

const assert = chai.assert;

describe('Testes de Usuário', () => {
  it('Teste: Requisição do JSON', async () => {
    const usersData = await users.get().catch((err) => console.error(`Ocorreu um erro ao fazer a requisição: ${err}`));
    assert.isArray(usersData, 'O objeto retornado não é um array');
    assert.lengthOf(usersData, 10, 'O Array não tem 10 posições');
    assert.hasAllDeepKeys(usersData[0], [ 'id', 'name', 'username', 'address', 'company', 'email', 'phone', 'website' ]);
  });

  it('Teste: Websites', async () => {
    const usersWebsites = await users.getWebsites().catch((err) => console.error(`Ocorreu um erro ao recuperar os websites: ${err}`));
    assert.isArray(usersWebsites, 'O objeto retornado não é um array');
    assert.lengthOf(usersWebsites, 10, 'O Array não tem 10 posições');
    assert.oneOf('demarco.info', usersWebsites, 'Site não foi encontrado');
  });

  it('Teste: Informações dos usuários', async () => {
    const usersInfo = await users.getUsersInfo().catch((err) => console.error(`Ocorreu um erro ao recuperar os websites: ${err}`));
    assert.isArray(usersInfo, 'O objeto retornado não é um array');
    assert.lengthOf(usersInfo, 10, 'O Array não tem 10 posições');
    assert.containsAllKeys(usersInfo[0], ['name', 'email', 'company'], 'Objeto não tem todas as chaves esperadas');
    assert.equal(usersInfo[0].name, 'Chelsey Dietrich', 'Ordenação dos itens não está correta');
  });

  it('Teste: Endereços com \'suite\'', async () => {
    const usersSuite = await users.filterAddress('suite').catch((err) => console.error(`Ocorreu um erro ao filtrar os endereços: ${err}`));
    assert.isArray(usersSuite, 'O objeto retornado não é um array');
    assert.lengthOf(usersSuite, 7, 'O Array não tem os 7 objetos esperados');
    assert.include(usersSuite[0].address.suite, 'Suite', 'Palavra \'suite\' não foi encontrada no objeto');
    assert.containsAllDeepKeys(usersSuite[0], ['id', 'name', 'username', 'email']);
  });
});
