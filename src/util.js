let util = {};

util.httpsGet = (url) => {
  return new Promise((resolve, reject) => {
    let http = require('https');

    http.get(url, (res) => {
      if (res.statusCode !== 200)
        return reject(`StatusCode: ${res.statusCode}`);

      res.setEncoding('utf8');
      let responseData = "";
      res.on('data', (chunk) => { responseData += chunk });
      res.on('end', () => {
        try {
          const parsedData = JSON.parse(responseData);
          return resolve(parsedData);
        } catch (err) {
          return reject(err.message);
        }
      })
      res.on('error', reject);
    })
  })
}

module.exports = util;